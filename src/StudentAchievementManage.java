package others.swing.p010;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 */
public class StudentAchievementManage {
    public StudentAchievementManage() {
    }

    public static void main(String[] args) {
        LoginFrame lf = new LoginFrame();//加载窗体
        lf.setVisible(true);//设置窗体可见
        //添加窗口监听以接受关闭事件
        lf.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
}

//学生成绩管理系统界面
class MainFrame extends JFrame implements ActionListener {
    MenuBar myMenuBar = new MenuBar();//菜单
    Menu myMenuFile, myMenuEdit, myMenuUser, myMenuAbout;
    MenuItem miNew, miOpen, miSave, miSaveAs, miExit;
    MenuItem miAdd, miEdit, miDel, miFind, miShow, miUser, miAbout;

    public JTable table = new JTable();//表格
    public DefaultTableModel mm;//表格模型

    MainFrame() {
        myMenuFile = new Menu("文件");
        miNew = new MenuItem("新建");
        miOpen = new MenuItem("打开");
        miSave = new MenuItem("保存");
        miExit = new MenuItem("退出");
        //miNew.enable(false);
        //miOpen.enable(false);
        //miSave.enable(false);
        //myMenuFile.add(miNew);
        //myMenuFile.add(miOpen);
        //myMenuFile.add(miSave);
        myMenuFile.add(miExit);
        myMenuEdit = new Menu("数据编辑");
        miFind = new MenuItem("查找数据");
        miAdd = new MenuItem("添加数据");
        miEdit = new MenuItem("修改数据");
        miDel = new MenuItem("删除数据");
        miShow = new MenuItem("全部显示");
        myMenuEdit.add(miFind);
        myMenuEdit.add(miAdd);
        myMenuEdit.add(miEdit);
        myMenuEdit.add(miDel);
        myMenuEdit.add(miShow);
        myMenuUser = new Menu("用户管理");
        miUser = new MenuItem("编辑用户");
        myMenuUser.add(miUser);
        myMenuAbout = new Menu("关于我");
        miAbout = new MenuItem("我的信息");
        myMenuAbout.add(miAbout);
        myMenuBar.add(myMenuFile);
        myMenuBar.add(myMenuEdit);
        myMenuBar.add(myMenuUser);
        myMenuBar.add(myMenuAbout);
        String[] col = {"学号", "姓名", "性别", "班级", "专业", "大学语文", "大学英语", "高等数学"};//创建属性列名
        mm = new DefaultTableModel(col, 0);
        table.setModel(mm);
        JScrollPane tableScrollPane = new JScrollPane(table);//设置滚动条
        this.setMenuBar(myMenuBar);
        this.add(tableScrollPane);
        miExit.addActionListener(this);//为各按钮注册事件监听器对象
        miFind.addActionListener(this);
        miAdd.addActionListener(this);
        miEdit.addActionListener(this);
        miDel.addActionListener(this);
        miShow.addActionListener(this);
        miUser.addActionListener(this);
        miAbout.addActionListener(this);
    }

    void freshTable(String sql) {
        MyConnection conn = new MyConnection();//获得数据库连接
        ResultSet rs;//保存查询返回结果对象
        rs = conn.getResult(sql);
        if (rs != null) {
            try {
                mm.setRowCount(0);
                table.setModel(mm);
                while (rs.next()) {
                    String 学号 = rs.getString("学号");
                    String 姓名 = rs.getString("姓名");
                    String 性别 = rs.getString("性别");
                    String 班级 = rs.getString("班级");
                    String 专业 = rs.getString("专业");
                    String 大学语文 = rs.getString("大学语文");
                    String 大学英语 = rs.getString("大学英语");
                    String 高等数学 = rs.getString("高等数学");
                    String[] cloumns = {学号, 姓名, 性别, 班级, 专业, 大学语文, 大学英语, 高等数学};
                    mm.addRow(cloumns);
                }
                //table.clearSelection();
                table.setModel(mm);
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }//捕获异常
    }

    //重载动作事件接口中的方法
    @Override
    public void actionPerformed(ActionEvent e) {
        //退出
        if (e.getSource() == miExit) {
            System.exit(0);
            //查找
        } else if (e.getSource() == miFind) {
            FindFrame ff = new FindFrame(this);
            ff.setVisible(true);
            //添加
        } else if (e.getSource() == miAdd) {
            AddFrame af = new AddFrame(this);
            af.setVisible(true);
            //修改
        } else if (e.getSource() == miEdit) {
            if (table.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "请选择你要修改的内容", "温馨提示", JOptionPane.INFORMATION_MESSAGE);//弹窗提示
            } else {
                EditFrame ef = new EditFrame(this);
                ef.学号.setText((String) table.getValueAt(table.getSelectedRow(), 0));
                ef.姓名.setText((String) table.getValueAt(table.getSelectedRow(), 1));
                ef.性别.setText((String) table.getValueAt(table.getSelectedRow(), 2));
                ef.班级.setText((String) table.getValueAt(table.getSelectedRow(), 3));
                ef.专业.setText((String) table.getValueAt(table.getSelectedRow(), 4));
                ef.大学语文.setText((String) table.getValueAt(table.getSelectedRow(), 5));
                ef.大学英语.setText((String) table.getValueAt(table.getSelectedRow(), 6));
                ef.高等数学.setText((String) table.getValueAt(table.getSelectedRow(), 7));
                ef.setVisible(true);
                this.freshTable("select * from 学生成绩表");//刷新表格
            }
            //删除
        } else if (e.getSource() == miDel) {
            if (table.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "请选择你要删除的行", "温馨提示", JOptionPane.INFORMATION_MESSAGE);

            } else {
                String sql = "delete from 学生成绩表 where 学号 = '" + table.getValueAt(table.getSelectedRow(), 0) + "'";
                //JOptionPane.showMessageDialog(null, sql, "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                MyConnection conn = new MyConnection();
                if (conn.executeSql(sql)) {
                    JOptionPane.showMessageDialog(null, "成功删除", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                    this.freshTable("select * from 学生成绩表");
                } else {
                    JOptionPane.showMessageDialog(null, "未知错误", "删除失败", JOptionPane.INFORMATION_MESSAGE);
                }
            }
            //显示
        } else if (e.getSource() == miShow) {
            //JOptionPane.showMessageDialog(null, "未知错误", "删除失败", JOptionPane.INFORMATION_MESSAGE);
            this.freshTable("select * from 学生成绩表");
            //编辑用户
        } else if (e.getSource() == miUser) {
            UserFrame uf = new UserFrame();
            uf.setVisible(true);
            //作业说明
        } else if (e.getSource() == miAbout) {
            AboutFrame af = new AboutFrame();
            af.setVisible(true);
        }
    }
}

class MyConnection {
    ResultSet re;
    String strurl = "jdbc:odbc:achievement";//achievement为数据源名称

    public MyConnection() {
    }

    public ResultSet getResult(String sql) {
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");//静态类
            Connection conn = DriverManager.getConnection(strurl, "", "");//地址
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);//被锁仍能被操作
            ResultSet re = stmt.executeQuery(sql);//有条件查找
            return re;
        } catch (Exception e) {
            e.printStackTrace();//异常处理
            return null;
        }
    }

    boolean executeSql(String sql) {
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            Connection conn = DriverManager.getConnection(strurl);
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            conn.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}

class AddFrame extends JDialog implements ActionListener {
    public static final int WIDTH = 400;
    public static final int HEIGHT = 400;
    JLabel 学号1, 姓名1, 性别1, 班级1, 专业1, 大学语文1, 大学英语1, 高等数学1;
    JTextField 学号, 姓名, 性别, 班级, 专业, 大学语文, 大学英语, 高等数学;
    JButton b;
    JPanel p;
    MainFrame mf;

    public AddFrame(MainFrame mmf) {
        setTitle("添加学生成绩");
        setSize(WIDTH, HEIGHT);
        setLocation(120, 180);
        Container contentPane = getContentPane();//创建一个容器对象
        contentPane.setLayout(new FlowLayout());//流式布局
        学号1 = new JLabel("学号");
        姓名1 = new JLabel("姓名");
        性别1 = new JLabel("性别");
        班级1 = new JLabel("班级");
        专业1 = new JLabel("专业");
        大学语文1 = new JLabel("大学语文");
        大学英语1 = new JLabel("大学英语");
        高等数学1 = new JLabel("高等数学");
        学号 = new JTextField(5);
        姓名 = new JTextField(5);
        性别 = new JTextField(5);
        班级 = new JTextField(5);
        专业 = new JTextField(5);
        大学语文 = new JTextField(5);
        大学英语 = new JTextField(10);
        高等数学 = new JTextField(10);
        b = new JButton("确定");
        p = new JPanel();
        p.setLayout(new GridLayout(10, 2, 5, 5));//表格布局
        p.add(学号1);
        p.add(学号);
        p.add(姓名1);
        p.add(姓名);
        p.add(性别1);
        p.add(性别);
        p.add(班级1);
        p.add(班级);
        p.add(专业1);
        p.add(专业);
        p.add(大学语文1);
        p.add(大学语文);
        p.add(大学英语1);
        p.add(大学英语);
        p.add(高等数学1);
        p.add(高等数学);
        p.add(new Label(""));
        p.add(new Label(""));
        p.add(b);
        contentPane.add(p);
        //添加按钮监听器
        b.addActionListener(this);
        mf = mmf;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (学号.getText().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "请输入学号", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
        } else if (姓名.getText().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "请输入姓名", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
        } else if (大学语文.getText().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "请输入大学语文", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
        } else {
            String sql = "select * from 学生成绩表 where 学号='" + 学号.getText() + "'";
            MyConnection conn = new MyConnection();
            ResultSet rs;
            rs = conn.getResult(sql);
            try {
                //System.out.println(rs.getRow());
                if (rs.next()) {
                    JOptionPane.showMessageDialog(null, "此学号已经存在", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                }//添加学生的时候，发现数据库已经有了，提示你学号已经存在，报错误信息
                else {
                    sql = "insert into 学生成绩表 values('" + 学号.getText() + "','" + 姓名.getText() + "','" + 性别.getText() + "','" + 班级.getText() + "','" + 专业.getText() + "','" + 大学语文.getText() + "','" + 大学英语.getText() + "','" + 高等数学.getText() + "')";
                    //实行数据库插入的语句
                    if (conn.executeSql(sql)) {
                        JOptionPane.showMessageDialog(null, "添加成功", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                        mf.freshTable("select * from 学生成绩表");
                        学号.setText("");
                        姓名.setText("");
                        性别.setText("");
                        班级.setText("");
                        专业.setText("");
                        大学语文.setText("");
                        大学英语.setText("");
                        高等数学.setText("");

                    } else {
                        JOptionPane.showMessageDialog(null, "添加失败", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                    }//插入失败
                }
            } catch (Exception er) {
                System.out.println(er.toString());
            }
        }
    }

}

class EditFrame extends JDialog implements ActionListener {
    public static final int WIDTH = 400;
    public static final int HEIGHT = 400;
    JLabel 学号1, 姓名1, 性别1, 班级1, 专业1, 大学语文1, 大学英语1, 高等数学1;
    JTextField 学号, 姓名, 性别, 班级, 专业, 大学语文, 大学英语, 高等数学;
    JButton b;
    JPanel p;
    MainFrame mf;

    public EditFrame(MainFrame mmf) {
        setTitle("修改学生信息");
        setSize(WIDTH, HEIGHT);
        setLocation(120, 180);
        Container contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());
        学号1 = new JLabel("学号");
        姓名1 = new JLabel("姓名");
        性别1 = new JLabel("性别");
        班级1 = new JLabel("班级");
        专业1 = new JLabel("专业");
        大学语文1 = new JLabel("大学语文");
        大学英语1 = new JLabel("大学英语");
        高等数学1 = new JLabel("高等数学");
        学号 = new JTextField(5);
        姓名 = new JTextField(5);
        性别 = new JTextField(5);
        班级 = new JTextField(5);
        专业 = new JTextField(5);
        大学语文 = new JTextField(5);
        大学英语 = new JTextField(10);
        高等数学 = new JTextField(10);
        学号.setEnabled(false);
        b = new JButton("确定");
        p = new JPanel();
        p.setLayout(new GridLayout(10, 2, 5, 5));
        p.add(学号1);
        p.add(学号);
        p.add(姓名1);
        p.add(姓名);
        p.add(性别1);
        p.add(性别);
        p.add(班级1);
        p.add(班级);
        p.add(专业1);
        p.add(专业);
        p.add(大学语文1);
        p.add(大学语文);
        p.add(大学英语1);
        p.add(大学英语);
        p.add(高等数学1);
        p.add(高等数学);
        p.add(new Label(""));
        p.add(new Label(""));
        p.add(b);
        contentPane.add(p);
        //添加按钮监听器
        b.addActionListener(this);
        mf = mmf;
    }//这是画界面

    @Override
    public void actionPerformed(ActionEvent e) {

        if (学号.getText().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "请输入学号", "温馨提示", JOptionPane.INFORMATION_MESSAGE);//弹出的小窗口提示
        } else if (姓名.getText().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "请输入姓名", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
        } else if (大学语文.getText().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "请输入大学语文", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
        } else {
            String sql = "update 学生成绩表 set 姓名='" + 姓名.getText() + "',性别='" + 性别.getText() + "',班级='" + 班级.getText() + "',专业='" + 专业.getText() + "',大学语文='" + 大学语文.getText() + "',大学英语='" + 大学英语.getText() + "',高等数学='" + 高等数学.getText() + "' where 学号='" + 学号.getText() + "'";
            //数据库语句
            MyConnection conn = new MyConnection();//打开数据库的连接
            try {
                //JOptionPane.showMessageDialog(null, sql, "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                //执行sql语句，弹出窗口，查询学生表刷新table
                if (conn.executeSql(sql)) {
                    JOptionPane.showMessageDialog(null, "修改成功", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                    mf.freshTable("select * from 学生成绩表");
                    this.dispose();//关闭当前窗体对象，隐藏
                } else {
                    JOptionPane.showMessageDialog(null, "修改失败", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception er) {
                er.printStackTrace();
            }
        }
    }

}

class FindFrame extends JDialog implements ActionListener {
    MainFrame mf;
    JPanel p;
    JComboBox c;
    JTextField t;
    JButton b;
    JButton fAll;
    String sql = "select * from student";
    String[] colStr = {"学号", "姓名", "性别", "班级", "专业", "大学语文", "大学英语", "高等数学"};

    public FindFrame(MainFrame mmf) {
        mf = mmf;
        p = new JPanel();
        c = new JComboBox(colStr);
        t = new JTextField(10);
        b = new JButton("查找");
        fAll = new JButton("全部显示");
        b.addActionListener(this);
        fAll.addActionListener(this);
        p.add(new JLabel("选择"));
        p.add(c);
        p.add(new JLabel("查找内容"));
        p.add(t);
        p.add(b);
        p.add(fAll);
        this.add(p);
        this.setTitle("查找");
        this.setSize(450, 80);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //查找
        if (e.getSource() == b) {
            String selectStr = c.getSelectedItem().toString();
            if (selectStr == "大学英语") {
                sql = "select * from 学生成绩表 where " + selectStr + " = " + t.getText().toString();
            } else {
                sql = "select * from 学生成绩表 where " + selectStr + " = '" + t.getText().toString() + "'";
            }
            mf.freshTable(sql);
            //全部显示
        }
        if (e.getSource() == fAll) {
            sql = "select * from 学生成绩表";
            mf.freshTable(sql);
        }//监听程序，选择查成绩，最后是重新刷新table
    }
}

//关于我的信息
class AboutFrame extends JDialog {
    AboutFrame() {
        JPanel p1 = new JPanel();
        p1.add(new JLabel("学生成绩管理系统"));
        p1.add(new JLabel("制作者: Jack魏"));
        p1.add(new JLabel("学院：计算机工程学院"));
        p1.add(new JLabel("电话：17788889999"));
        p1.setLayout(new GridLayout(4, 1, 5, 5));
        this.add(p1);
        this.setTitle("关于我");
        this.setSize(300, 200);
    }
}

//用户密码
class UserFrame extends JFrame implements ActionListener {
    JTextField user, pass;
    JButton add, del;
    JTable t;
    JPanel p1, p2, p3, p4, p5;
    DefaultTableModel m;

    public UserFrame() {
        p1 = new JPanel();
        p2 = new JPanel();
        p3 = new JPanel();
        p4 = new JPanel();
        p5 = new JPanel();
        user = new JTextField(8);
        pass = new JTextField(8);
        add = new JButton("添加");
        del = new JButton("删除");
        String[] col = {"用户名", "密码"};
        m = new DefaultTableModel(col, 0);
        t = new JTable();
        t.setModel(m);
        JScrollPane sp = new JScrollPane(t);
        p1.add(new JLabel("用户名"));
        p1.add(user);
        p1.add(new JLabel("密码"));
        p1.add(pass);
        p1.add(add);
        p2.add(sp);
        p3.add(del);
        add.addActionListener(this);
        del.addActionListener(this);
        MyConnection conn = new MyConnection();
        ResultSet rs;
        rs = conn.getResult("select * from 管理员");//开始时连接数据库，查询数据，返回结果集，
        if (rs != null) {//把查询结果放到table里面
            try {
                //m.setRowCount(0);
                //table.setModel(mm);
                while (rs.next()) {
                    String 用户名 = rs.getString("用户名");
                    String 密码 = rs.getString("密码");
                    String[] cloumns = {用户名, 密码};
                    m.addRow(cloumns);
                }
                t.setModel(m);
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
        this.add(p1, BorderLayout.NORTH);
        this.add(p2, BorderLayout.CENTER);
        this.add(p3, BorderLayout.SOUTH);
        this.add(p4, BorderLayout.WEST);
        this.add(p5, BorderLayout.EAST);
        this.setTitle("用户管理");
        this.setSize(600, 400);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //用户添加
        if (e.getSource() == add) {
            if (user.getText().toString().equals("")) {
                JOptionPane.showMessageDialog(null, "请输入用户名", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
            } else if (pass.getText().toString().equals("")) {
                JOptionPane.showMessageDialog(null, "请输入密码", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
            } else {
                MyConnection conn = new MyConnection();
                ResultSet rs;
                try {
                    rs = conn.getResult("select * from 管理员 where 用户名='" + user.getText().toString() + "'");
                    if (rs.next()) {
                        JOptionPane.showMessageDialog(null, "此用户已经存在", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        if (conn.executeSql("insert into 管理员 values('" + user.getText().toString() + "','" + pass.getText().toString() + "')")) {
                            String[] newUser = {user.getText(), pass.getText()};
                            m.addRow(newUser);
                            t.setModel(m);
                            JOptionPane.showMessageDialog(null, "添加成功", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(null, "添加失败", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                } catch (Exception er) {
                    System.out.println(er.toString());
                }
            }

            //删除
        } else if (e.getSource() == del) {
            if (t.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(null, "请选择你要删除的行", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
            } else {
                String sql = "delete from 管理员 where 用户名 = '" + t.getValueAt(t.getSelectedRow(), 0) + "'";
                //JOptionPane.showMessageDialog(null, sql, "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                MyConnection conn = new MyConnection();
                if (conn.executeSql(sql)) {
                    m.removeRow(t.getSelectedRow());
                    t.setModel(m);
                    //t.removeRowSelectionInterval();
                    JOptionPane.showMessageDialog(null, "成功删除", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "未知错误", "删除失败", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
    }
}

//登陆
class LoginFrame extends JDialog implements ActionListener {
    JPanel p;
    JTextField user, pass;
    JButton login, cancel;

    public LoginFrame() {
        // 背景图片地址，如果是java指令运行需要使用下面的：
        String fileName = "index.jpg";
        // 如果是IDEA运行，请使用如下
        // String fileName = "src/others/swing/p010/index.jpg";
        p = new JPanel();
        user = new JTextField(10);
        pass = new JTextField(10);
        login = new JButton("登录");
        cancel = new JButton("退出");
        user.setText("");
        pass.setText("");
        login.addActionListener(this);
        cancel.addActionListener(this);
        p.add(new JLabel("账号"));
        p.add(user);
        p.add(new JLabel("密码"));
        p.add(pass);
        p.add(login);
        p.add(cancel);
        this.add(p);
        this.setTitle("学生成绩管理系统");
        JLabel img = new JLabel(new ImageIcon(fileName));
        p.add(img);
        this.setSize(700, 600);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //查找
        if (e.getSource() == login) {
            if (user.getText().toString().equals("")) {
                JOptionPane.showMessageDialog(null, "请输入用户名", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
            } else if (pass.getText().toString().equals("")) {
                JOptionPane.showMessageDialog(null, "请输入密码", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
            } else {
                MyConnection conn = new MyConnection();
                ResultSet rs;
                String sql = "select * from 管理员 where 用户名 = '" + user.getText().toString() + "' and 密码 = '" + pass.getText().toString() + "'";
                try {
                    rs = conn.getResult(sql);
                    if (rs.next()) {
                        this.dispose();
                        //JOptionPane.showMessageDialog(null, "此用户已经存在", "温馨提示", JOptionPane.INFORMATION_MESSAGE);
                        sql = "select * from 学生成绩表";
                        MainFrame mf = new MainFrame();
                        mf.setTitle("学生成绩管理系统");
                        mf.setSize(600, 486);
                        mf.freshTable(sql);
                        mf.setVisible(true);
                        mf.addWindowListener(new WindowAdapter() {
                            public void windowClosing(WindowEvent e) {
                                System.exit(0);
                            }
                        });
                    } else {
                        JOptionPane.showMessageDialog(null, "用户名或密码错误", "登录失败", JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (Exception er) {
                    System.out.println(er.toString());
                }
            }
            //全部显示
        }
        if (e.getSource() == cancel) {
            System.exit(0);
        }
    }
}
