# 基于Java Swing的学生成绩管理系统

## 介绍
为应对学生成绩的查询，学生成绩管理系统包括以下功能：

1．用户通过帐号和密码登入界面。
2．登陆界面后，出现学生成绩的各个功能使用。
3．主要的功能模块为：文件管理、数据编辑、用户管理、关于我的个人信息。
4．在各个功能模块中又有小的功能，如：
数据编辑包括：查找信息、添加信息、修改信息、删除信息、信息的全部显示。
用户管理包括：对用户的帐号和密码的添加和删除。
关于我中是我的个人信息。
5．另外使用了Access数据库的管理：学生基本成绩信息管理、用户基本信息管理。

## 相关技术
1.  Java的Swing编程
2.  Java的JDBC编程


## 博客地址

[https://blog.csdn.net/WeiHao0240/article/details/121206444](https://blog.csdn.net/WeiHao0240/article/details/121206444)


## 所需环境
1.  JDK1.7
2.  Access 2016


## 安装教程
遇到问题可以到**相关博客**进行查看

1.  **运行之前需要配置好ODBC**
2.  检查JDK版本，必须是JDK1.7：
```
java -version
```

3.  进入`src`目录，编译
```
javac -encoding UTF-8 StudentAchievementManage.java
```

4.  运行
```
java StudentAchievementManage
```

注意：如果修改源代码后需要删除所有.class文件，在编译运行！
```
del *.class
```


## 运行截图

1.登录

![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/095733_bcade2ec_1590078.png "1-登录.png")

2.成绩信息

![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/095741_0c5dbc64_1590078.png "2-成绩信息.png")

3.查找

![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/095746_389fd65d_1590078.png "3-查找.png")

4.添加

![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/095752_3911faa2_1590078.png "4-添加.png")

5.修改

![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/095759_b2e59f16_1590078.png "5-修改.png")

6.删除

![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/095806_f21a4c58_1590078.png "6-删除.png")

7.管理员用户

![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/095812_8e5bf648_1590078.png "7-管理员用户.png")

8.关于我

![输入图片说明](https://images.gitee.com/uploads/images/2021/1110/095819_4531b6c3_1590078.png "8-关于我.png")


## 相关博客
1.  [Microsoft Access 2016安装教程](https://blog.csdn.net/WeiHao0240/article/details/120672363)
2.  [Java使用ODBC连接Access数据库](https://blog.csdn.net/WeiHao0240/article/details/120727203)
3.  [TextPad安装环境配置](https://jackwei.blog.csdn.net/article/details/86914950)
4.  [IDEA运行Java Swing项目中文乱码](https://blog.csdn.net/WeiHao0240/article/details/120744954)
5.  [Java指令编译java文件](https://blog.csdn.net/WeiHao0240/article/details/120778832)
6.  [Java运行程序找不到ODBC驱动](https://blog.csdn.net/WeiHao0240/article/details/120879107)